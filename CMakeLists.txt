cmake_minimum_required(VERSION 3.20)
project(qt_override_openurl)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_C_STANDARD 11)

find_package(Qt5 COMPONENTS Gui REQUIRED)

add_library(qt_override_openurl SHARED QDesktopServices_openUrl.cpp)
target_link_libraries(qt_override_openurl Qt5::Gui)

add_library(qt_override_openurl_stub SHARED QDesktopServices_openUrl_stub.c)
target_link_libraries(qt_override_openurl_stub dl)

install(TARGETS qt_override_openurl qt_override_openurl_stub DESTINATION lib)
